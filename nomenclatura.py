// Nomenclatura de variáveis

const categories = [
  {
    title: 'User',
    followers: 5
  },
  {
    title: 'Friendly',
    followers: 50,
  },
  {
    title: 'Famous',
    followers: 500,
  },
  {
    title: 'Super Star',
    followers: 1000,
  },
]

export default async function getFolowersFromUser(req, res) {
  const githubUser = String(req.query.username)

  if (!githubUser) {
    return res.status(400).json({
      message: `Please provide an username to search on the github API`
    })
  }

  const responseFromGitHub = await fetch(`https://api.github.com/users/${githubUser}`);

  if (responseFromGitHub.status === 404) {
    return res.status(400).json({
      message: `User with username "${githubUser}" not found`
    })
  }

  const user = await responseFromGitHub.json()

  const orderCategories = categories.sort((a, b) =>  b.followers - a.followers);

  const category = orderCategories.find(i => user.followers > i.followers)

  const result = {
    githubUser,
    category: category.title
  }

  return result
}

getFolowersFromUser({ query: {
  username: 'josepholiveira'
}}, {})

